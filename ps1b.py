annual_salary = int(input("Enter your annual salary: "))
portion_saved = float(input("Enter the percent of your salary to save, as a decimal: "))
total_cost = int(input("Enter the cost of your dream home:​ "))
semi_annual_raise = float(input("Enter the semi­annual raise, as a decimal:​ "))

number_of_months = 0
portion_down_payment = .25 * total_cost
current_savings = 0
r = 0.04


while current_savings < portion_down_payment:
    current_savings += current_savings*r/12 + portion_saved*annual_salary/12
    number_of_months += 1
    if number_of_months % 6 == 0:
        annual_salary = annual_salary*(1+semi_annual_raise)


print("Number of months:", number_of_months)